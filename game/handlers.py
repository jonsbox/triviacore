import asyncio
import aiohttp
import time
import json
from typing import Optional, List
from datetime import datetime
from operator import itemgetter
from collections import defaultdict
from functools import partial
from tornado.web import MissingArgumentError
from peewee import JOIN, fn
from playhouse.shortcuts import model_to_dict
from base_handler import BaseHandler
from models import db_objects, Theme, Game, Gamer, GamersQueue, Question, Round, RoundMove, ThemeRating,\
    RecordsMan


def get_game_themes(theme: Theme) -> str:
    """

    :param theme: тема или None
    :return: название темы или "по всем темам"
    """
    return theme.name if theme else 'По всем темам'


class StartAppHandler(BaseHandler):
    @staticmethod
    async def get_global_stat(gamer: int) -> Optional[dict]:
        """

        :param gamer: id игрока для личной статистики
        :return: та же статистика "по всем темам", что и по конкретной теме
        """
        try:
            global_record = await db_objects.execute(
                RecordsMan.select(
                    RecordsMan.score,
                    Gamer.id,
                    Gamer.name
                ).join(Gamer).where(RecordsMan.theme >> None).limit(1).dicts()
            )
            global_record = global_record[0]
            personal_record = await db_objects.execute(
                ThemeRating.select(ThemeRating.record).where(
                    (ThemeRating.gamer == gamer) &
                    (ThemeRating.theme >> None)
                ).limit(1).tuples()
            )
            personal_record = personal_record[0][0] if len(personal_record) else 0
            total_questions = await db_objects.scalar(
                Question.select(fn.count(Question.id)).join(Theme).where(
                    (Theme.name != 'Модерация') &
                    Question.active
                )
            )
            return {
                "id": 0,
                "name": "По всем темам",
                "lang": "ru",
                "path_to_img": 'media/images/diversity-154704_640.png',
                "total_answers": 0,
                "total_questions": total_questions,
                "score": global_record['score'],
                'personal_score': personal_record,
                "recordsman_id": global_record['id'],
                "recordsman_name": global_record['name']
            }
        except IndexError:
            return None

    @staticmethod
    async def _format_games(rows: List[Game]) -> list:
        """

        :param rows:
        :return:
        """
        # done_rounds = set()
        # single, multi = {}, {}
        # for r in rows:
        #     res = single if r.status is None else multi
        #     if r.id not in res:
        #         res[r.id] = {
        #             "id": r.id,
        #             "start_time": time.mktime(r.start.timetuple()),
        #             "finish_time": 0,
        #             "theme": get_game_themes(r.theme),
        #             "score": 0,
        #             "global_score": 1000,
        #             "rounds": []
        #         }
        #     if not hasattr(r, 'round'):
        #         continue
        #     if r.round.id in done_rounds:
        #         continue
        #     done_rounds.add(r.round.id)
        #     question = r.round.question
        #     game_round = {"id": r.round.id, "round_num": r.round.round_number, "question": {
        #         "theme": question.theme.name,
        #         "text": question.question,
        #         "img_url": question.question_img,
        #         "timer": question.timer,
        #         "difficulty": question.complexity,
        #         'answers': [{
        #             "id": n,
        #             "text": getattr(question, f'answer_{n}'),
        #             "img_url": getattr(question, f'answer_{n}_img'),
        #             "correct": str(n) == question.correct
        #         } for n in range(1, 5)]
        #     }, "score": 0}
        #     if hasattr(r.round, 'roundmove'):
        #         game_round.update({
        #             "answer": r.round.roundmove.answer,
        #             "timer_left": r.round.roundmove.timer_left or 0,
        #         })
        #     else:
        #         game_round.update({
        #             "answer": None,
        #             "timer_left": 0,
        #         })
        #     res[r.id]['rounds'].append(game_round)
        # return list(single.values()), list(multi.values())
        games = []
        for r in rows:
            rounds = await db_objects.execute(Round.get_rounds_query(r))
            gamers = {r.moving_user}
            for t in rounds:
                if hasattr(t, 'roundmove'):
                    gamers.add(t.roundmove.gamer)
            games.append(Game.serialize(r, gamers, r.theme, rounds))
        return games

    @classmethod
    async def _get_themes(cls, gamer_id: int) -> List[Theme]:
        """

        :param gamer_id: id игрока
        :return: список активных и отмодерированных тем
        """
        # themes = await db_objects.execute(
        #     Theme.select(Theme, fn.count(Question.id).alias('total_questions'),
        #                  RecordsMan.score, Gamer.id.alias('recordsman_id'),
        #                  Gamer.name.alias('recordsman_name'),
        #                  fn.coalesce(ThemeRating.record, 0).alias('personal_score')).where(
        #         (Theme.name != 'Модерация')
        #     ).join(Question).switch(Theme).join(RecordsMan, JOIN.LEFT_OUTER).join(Gamer, JOIN.LEFT_OUTER).
        #         switch(Theme).join(ThemeRating, JOIN.LEFT_OUTER, on=(
        #         (ThemeRating.theme == Theme.id) &
        #         (ThemeRating.gamer == gamer_id)
        #     )).group_by(Theme, RecordsMan.score, Gamer.id, Gamer.name, ThemeRating.record).dicts()
        # )
        # themes = list(themes)
        # global_stat = await cls.get_global_stat(gamer_id)
        # if global_stat:
        #     themes.append(global_stat)
        themes = list(Theme.select().where(Theme.name != 'Модерация'))
        fake = Theme()
        fake.id = 0
        fake.name = 'По всем темам'
        fake.lang = 'ru'
        fake.path_to_img = None
        themes.append(fake)
        return list(map(Theme.serialize, themes))

    @staticmethod
    async def _my_questions(gamer_id: int) -> dict:
        """

        :param gamer_id:
        :return: информаия о вопросах, присланных игороком через приложение
        """
        total = await db_objects.execute(
            Question.select(Question.active, Theme.name).join(Theme).where(
                Question.author == gamer_id
            ).tuples()
        )
        moderated = filter(lambda q: q[0] and q[1] != 'Модерация', total)
        return {
            'total': len(total),
            'moderated': len(list(moderated))
        }

    @staticmethod
    async def _update_token(gamer_id, token):
        gamer = await db_objects.get(Gamer, id=gamer_id)
        if gamer.device_token != token:
            gamer.device_token = token
            await db_objects.update(gamer)

    async def post(self):
        try:
            data = self.data
            gamer = data['user']['id']
            if 'device_token' in data['user']:
                await self._update_token(gamer, data['user']['device_token'])

            mu = Gamer.alias()
            games = await db_objects.execute(
                Game.select(Game, Theme, mu).join(Theme, JOIN.LEFT_OUTER).
                switch(Game).join(GamersQueue, JOIN.LEFT_OUTER).switch(Game).join(mu, JOIN.LEFT_OUTER).where(
                    (Game.end >> None) &
                    (
                        (GamersQueue.gamer == gamer) |
                        (Game.moving_user == gamer)
                    )
                )
            )
            # single, multi = self._format_games(games)
            games = await self._format_games(list(games))
            await self.ok({
                # 'games': single,
                'games': games,
                # 'games_multi': multi,
                'themes': await self._get_themes(gamer),
                'my_questions': await self._my_questions(gamer)
            })
        except ValueError:
            await self.error('Игрок не указан')


class NewGameHandler(BaseHandler):
    @staticmethod
    async def _get_themes_single(gamer_id):
        themes = await db_objects.execute(
            Theme.select().join(Question).where(
                ~(Theme.id << (
                    Game.select(Game.theme).where(
                        (Game.end >> None) &
                        (Game.moving_user == gamer_id) &
                        (Game.status >> None)
                    )
                )) & (Theme.name != 'Модерация')
            ).distinct().dicts()
        )
        return list(map(Theme.serialize, themes))

    @staticmethod
    async def _add_to_queue_multi(game, gamer_id):
        async with db_objects.atomic():
            game.status = 'g'
            game.moving_user = gamer_id
            await db_objects.update(game)
            await db_objects.create(GamersQueue,
                              game=game,
                              gamer=gamer_id,
                              order=2)

    @staticmethod
    async def get_rounds_multi(game):
        # rows = await db_objects.execute(
        #     RoundMove.select(
        #         Round.id,
        #         Round.round_number,
        #         Theme.name.alias('theme'),
        #         Question.question.alias('text'),
        #         Question.question_img.alias('img_url'),
        #         Question.timer,
        #         Question.correct,
        #         Question.complexity.alias('difficulty'),
        #         Question.answer_1, Question.answer_2, Question.answer_3, Question.answer_4,
        #         Question.answer_1_img, Question.answer_2_img, Question.answer_3_img, Question.answer_4_img,
        #         RoundMove.correct.alias('opponent_correct'),
        #         RoundMove.answer.alias('opponent_answer'),
        #         RoundMove.score,
        #         Gamer.name.alias('gamer')
        #     ).join(Round).join(Question).join(Theme).switch(RoundMove).join(Gamer).where(
        #         Round.game == game
        #     ).dicts()
        # )
        # rounds = [{
        #     "id": r['id'], "round_num": r['round_number'], 'gamer': r['gamer'], "question": {
        #         "theme": r['theme'],
        #         "text": r['text'],
        #         "img_url": r['img_url'],
        #         "timer": r['timer'],
        #         "difficulty": r['difficulty'],
        #         'opponent_correct': r['opponent_correct'],
        #         'opponent_answer': r['opponent_answer'],
        #         'answers': [{
        #             "id": n,
        #             "text": r[f'answer_{n}'],
        #             "img_url": r[f'answer_{n}_img'],
        #             "correct": str(n) == r['correct']
        #         } for n in range(1, 5)]
        #     }, "score": r['score']
        # } for r in rows]
        rounds = await db_objects.execute(Round.get_rounds_query(game))
        return Round.serialize(rounds)

    @classmethod
    async def _get_themes_multi_or_game(cls, gamer_id):
        game = await db_objects.execute(
            Game.select(Game).join(GamersQueue).where(
                Game.status == 'w'
            ).limit(1)
        )
        if len(game):
            game = game[0]
            await asyncio.get_event_loop().create_task(cls._add_to_queue_multi(game, gamer_id))
            rounds = await db_objects.execute(Round.get_rounds_query(game))
            gamers = await db_objects.execute(Gamer.select().join(GamersQueue).where(GamersQueue.game == game))
            return Game.serialize(game, gamers, None, rounds)
            # return ({
            #     'game_exists': True,
            #     'game': {
            #         "id": game.id,
            #         "start_time": time.mktime(game.start.timetuple()),
            #         "finish_time": None,
            #         "theme": None,
            #         "score": 0,
            #         "global_score": None,
            #         "rounds": await cls.get_rounds_multi(game),
            #         "base_rounds": None
            #     },
            #     'themes': None
            # })
        else:
            themes = await db_objects.execute(
                Theme.select().order_by(fn.random()).limit(3).dicts()
            )
            return {
                'game_exists': False,
                'game': None,
                'themes': list(themes)
            }

    async def get(self):
        try:
            gamer_id = self.get_argument('user')
        except MissingArgumentError:
            await self.error('Не указан пользователь')
            return

        is_multi = bool(int(self.get_argument('contest', '0')))
        if is_multi:
            await self.ok(await self._get_themes_multi_or_game(gamer_id))
        else:
            await self.ok({
                'theme': await self._get_themes_single(gamer_id)
            })

    @staticmethod
    async def _check_common_single(gamer_id: int, theme: int) -> tuple:
        if theme:
            try:
                theme = await db_objects.get(Theme, id=theme)
            except Theme.DoesNotExist:
                return 'Тема не найдена', None

            started_games = await db_objects.execute(
                Game.select(Theme.name).join(Theme).switch(Game).join(GamersQueue, JOIN.LEFT_OUTER).where(
                    (Game.end >> None) &
                    (Game.moving_user == gamer_id) &
                    (Game.theme == theme) &
                    (Game.status >> None)
                ).tuples()
            )
            if len(started_games):
                return 'По данной теме уже есть игра', None
            else:
                return None, theme
        else:
            started_games = await db_objects.execute(
                Game.select(Game.theme).join(GamersQueue, JOIN.LEFT_OUTER).where(
                    (Game.end >> None) &
                    (Game.moving_user == gamer_id) &
                    (Game.theme >> None) &
                    (Game.status >> None)
                ).tuples()
            )
            if len(started_games):
                return 'У вас уже есть игра по всем темам', None
            return None, None

    @staticmethod
    async def _check_common_multi(gamer_id: int) -> tuple:
        try:
            await db_objects.get(Game,
                                 end=None,
                                 status='s',
                                 moving_user=gamer_id)
            return 'У вас уже есть игра, ожидающая выбор темы', None
        except Game.DoesNotExist:
            return None, None

    @classmethod
    async def _check_common(cls, gamer_id: int, theme: int, contest: bool) -> tuple:
        """
        Проверка параметров обычной игры при создании
        :param gamer_id:
        :param theme:
        :param contest: True - многопользовательская, False - одиночная
        :return: (error, theme)
        """
        if contest:
            return await cls._check_common_multi(gamer_id)
        else:
            return await cls._check_common_single(gamer_id, theme)

    @staticmethod
    async def _check_overrecord(gamer_id: int, theme_id: int) -> tuple:
        """
        Проверка параметров игры с попыткой побить рекорд
        :param gamer_id:
        :param theme_id:
        :return: (error, Theme, игра для побития рекорда)
        """
        try:
            if theme_id:
                theme = await db_objects.get(Theme, id=theme_id)
            else:
                theme = None
        except Theme.DoesNotExist:
            return 'Тема не найдена', None, None
        try:
            if theme_id:
                where_record = RecordsMan.theme == theme_id
                where_overrecord = Game.theme == theme
            else:
                where_record = RecordsMan.theme >> None
                where_overrecord = Game.theme >> None

            recordsman = await db_objects.execute(
                RecordsMan.select().where(where_record).dicts()
            )
            recordsman = recordsman[0]
        except IndexError:
            return 'Рекорд по этой теме не найден', None, None

        if recordsman['gamer'] == gamer_id:
            return 'Это рекорд вашей игры', None, None

        my_games = await db_objects.execute(
            Game.select().where(
                (Game.moving_user == gamer_id) &
                (Game.end >> None) &
                where_overrecord
            ).limit(1)
        )
        if len(my_games):
            error = 'данной теме' if theme_id else 'всем вопросам'
            return f'У вас уже есть игра по {error}', None, None

        overrecord = await db_objects.execute(
            Game.select().where(
                (Game.overrecord_game == recordsman['game']) &
                (Game.moving_user == gamer_id)
            )
        )
        if len(overrecord):
            return 'Вы уже пытались побить рекорд по этой теме', None, None
        return None, theme, recordsman['game']

    @classmethod
    async def _check(cls, gamer_id: int, theme_id: int, contest: bool, overrecord: bool) -> tuple:
        """
        Проверка параметров создаваемой игры
        :param gamer_id:
        :param theme_id:
        :param contest: является ли игра много пользовательской
        :param overrecord: пытаются ли побить рекор по теме
        :return: (error: str, Them, Game в которой пытаются побить рекорд)
        """
        if overrecord:
            error, theme, overrecord_game = await cls._check_overrecord(gamer_id, theme_id)
        else:
            error, theme = await cls._check_common(gamer_id, theme_id, contest)
            overrecord_game = None
        return error, theme, overrecord_game

    @staticmethod
    async def _create_game(gamer: int, theme: Theme, contest: bool, overrecord: Game) -> tuple:
        """
        Асинхронная задача для создания игры в транзакции
        :param gamer:
        :param theme:
        :param contest:
        :param overrecord:
        :return: (созданный объект игры, присоединялся ли к текущей многопользовательской)
        Если второй параметр True, то надо добавить информацию о сыгранном раунде
        """
        connected_multi = False
        if contest:
            try:
                game = await db_objects.execute(
                    Game.select().join(GamersQueue).where(
                        (Game.end >> None) &
                        (Game.status == 'w') &
                        (GamersQueue.gamer != gamer)
                    ).order_by(Game.start).limit(1)
                )
                game = game[0]
                async with db_objects.atomic():
                    game.moving_user = gamer
                    await db_objects.update(game)
                    await db_objects.create(GamersQueue,
                                            game=game.id,
                                            gamer=gamer,
                                            order=2)
                connected_multi = True
            except IndexError:
                async with db_objects.atomic():
                    game = await db_objects.create(Game,
                                                   moving_user=gamer,
                                                   status='s')
                    await db_objects.create(GamersQueue,
                                            game=game.id,
                                            gamer=gamer,
                                            order=1)
        else:
            async with db_objects.atomic():
                game = await db_objects.create(Game,
                                               moving_user=gamer,
                                               theme=theme,
                                               overrecord_game=overrecord)
        return game, connected_multi

    @staticmethod
    async def _get_global_score(theme: Optional[int]) -> int:
        """
        Глобальный рекорд по теме или в общем зачёте
        :param theme:
        :return:
        """
        try:
            r = await db_objects.get(RecordsMan, theme=theme)
            return r.score
        except RecordsMan.DoesNotExist:
            return 0

    async def post(self):
        try:
            data = self.data
            gamer = data['user']['id']
            theme = data['theme']
            contest = data.get('contest', False)
            overrecord = data.get('overrecord', False)
            error, theme, overrecord_game = await self._check(gamer, theme, contest, overrecord)
            if error:
                await self.error(error)
                return

            loop = asyncio.get_event_loop()
            game, connected_multi = await loop.create_task(self._create_game(gamer, theme, contest, overrecord_game))
            # if overrecord_game:
            #     base_rounds = await db_objects.scalar(
            #         Round.select(fn.max(Round.round_number)).where(Round.game == overrecord_game)
            #     )
            # else:
            #     base_rounds = None

            rounds = await db_objects.execute(Round.get_rounds_query(game))
            # rounds = await self.get_rounds_multi(game) if connected_multi else []

            gamer_obj = await db_objects.get(Gamer, id=gamer)
            await self.ok(Game.serialize(game, [gamer_obj], theme, rounds))
        except KeyError as e:
            await self.error(f'Не указан {e.args[0]}')


class GetQuestionHandler(BaseHandler):
    @staticmethod
    async def _check(gamer: int, game: Game) -> Optional[str]:
        try:
            game = await db_objects.get(Game, id=game)
            if game.end:
                return 'Игра окончена'
            if game.moving_user_id != gamer:
                return 'Не ваша очередь'
            asked = await db_objects.execute(
                RoundMove.select().join(Round).where(
                    (Round.game == game) &
                    (RoundMove.gamer == gamer) &
                    (RoundMove.answer >> None)
                ).limit(1)
            )
            if len(asked):
                return 'У вас есть вопросы без ответа в этой игре'
        except Game.DoesNotExist:
            return 'Игра не найдена'

    @staticmethod
    async def _get_random_question(gamer: int, game: Game) -> Optional[Question]:
        """
        Получить следующий случайный вопрос
        :param gamer:
        :param game:
        :return:
        """
        theme = game.theme_id
        if theme:  # у игры есть темы
            question = await db_objects.execute(
                Question.select(Question, Theme).join(Theme).where(
                    (Question.theme == theme) &
                    ~(Question.id << (
                        Question.select(Question.id).join(Round).join(RoundMove).where(
                            (RoundMove.gamer == gamer) &
                            (Round.game == game)
                        )
                    )) &
                    Question.active
                ).order_by(fn.random()).limit(1)
            )
        else:  # вопросы из любой темы
            question = await db_objects.execute(
                Question.select().join(Theme).where(
                    (Theme.name != 'Модерация') &
                    Question.active &
                    ~(Question.id << (
                        Question.select(Question.id).join(Round).join(RoundMove).where(
                            (RoundMove.gamer == gamer) &
                            (Round.game == game)
                        )
                    ))
                ).order_by(fn.random()).limit(1)
            )
        # TODO: отсекать уже задавашиеся вопросы игроку
        try:
            return question[0]
        except IndexError:
            return None

    @staticmethod
    async def _create_round_for_single(gamer: int, game: Game, question: Question, round_num: int) -> Round:
        """
        Асинхронная задача для создания раунда одиночной игры в транзакции
        :param gamer:
        :param game:
        :param question:
        :param round_num:
        """
        async with db_objects.atomic():
            game_round = await db_objects.create(Round,
                                                 game=game,
                                                 round_number=round_num,
                                                 question=question)
            await db_objects.create(RoundMove,
                                    round=game_round,
                                    gamer=gamer)
        return game_round

    @staticmethod
    async def _get_round_num(game: Game) -> int:
        """
        Получить номер вновь создаваемого раунда
        :param game:
        :return:
        """
        round_num = await db_objects.scalar(
            Round.select(fn.count(Round.round_number)).where(Round.game == game)
        ) or 0
        return round_num + 1

    async def _create_question_common(self, gamer: int, game: Game) -> tuple:
        """
        Получить вопрос для обычной однопользовательской игры
        :param gamer:
        :param game:
        :return: (Round, str)
        """
        total_gamers = await db_objects.scalar(
            GamersQueue.select(fn.max(GamersQueue.order)).where(GamersQueue.game == game)
        )
        question = await self._get_random_question(gamer, game)
        if not question:
            return None, 'В игре не осталось вопросов'
        loop = asyncio.get_event_loop()
        if total_gamers is None:
            round_num = await self._get_round_num(game)
            round = await loop.create_task(self._create_round_for_single(gamer, game, question, round_num))
        else:
            pass

        return round, None

    async def _create_question_overrecord(self, gamer: int, game: Game) -> tuple:
        """
        Получить вопрос для игры с попыткой побить рекорд
        :param gamer:
        :param game:
        :return: (Question, error)
        """
        round_num = await self._get_round_num(game)
        origin_rounds_num = await db_objects.scalar(
            Round.select(fn.count(Round.round_number)).where(Round.game == game.overrecord_game)
        )
        if round_num <= origin_rounds_num:
            question = await db_objects.execute(
                Question.select().join(Round).where(
                    (Round.game == game.overrecord_game) &
                    (Round.round_number == round_num)
                ).limit(1)
            )
            question = question[0]
            loop = asyncio.get_event_loop()
            round = await loop.create_task(self._create_round_for_single(gamer, game, question, round_num))
            return round, None
        else:
            question = await self._get_random_question(gamer, game)
            if not question:
                return None, 'В игре не осталось вопросов'
            loop = asyncio.get_event_loop()
            round = await loop.create_task(self._create_round_for_single(gamer, game, question, round_num))
            return round, None

    async def _create_question(self, gamer: int, game: Game) -> tuple:
        """
        Получить вопрос
        :param gamer:
        :param game:
        :return: (Round, str)
        """
        if game.overrecord_game:
            return await self._create_question_overrecord(gamer, game)
        else:
            return await self._create_question_common(gamer, game)

    @staticmethod
    def format_question(question: Question, round_num: int) -> dict:
        """
        Приводит к клиентскому формату
        :param question:
        :param round_num:
        :return:
        """
        data = {"id": question.id, "round_num": round_num, "question": {
            "theme": question.theme.name,
            "theme_id": question.theme.id,
            "theme_img_url": question.theme.path_to_img,
            "text": question.question,
            "img_url": question.question_img,
            "timer": question.timer,
            "difficulty": question.complexity,
            'answers': [{
                "id": n,
                "text": getattr(question, f'answer_{n}'),
                "img_url": getattr(question, f'answer_{n}_img'),
                "correct": str(n) == question.correct
            } for n in range(1, 5)]
        }, "answer": None, "timer_left": question.timer or 0, "score": 0}
        return data

    async def get(self):
        try:
            gamer = int(self.get_argument('user'))
            game = await db_objects.get(Game, id=int(self.get_argument('game')))
        except MissingArgumentError as e:
            await self.error(f'Не указан {e.arg_name}')
            return
        except Game.DoesNotExist:
            await self.error('Игра не найдена')
            return

        error = await self._check(gamer, game)
        if error:
            await self.error(error)
            return

        round, error = await self._create_question(gamer, game)
        if round:
            # await self.ok(self.format_question(question, round_num))
            await self.ok(Round.serialize([round]))
        else:
            await self.error(error)


class AnswerHandler(BaseHandler):
    @staticmethod
    async def _check(gamer: int, game: int, answer: int) -> tuple:
        """

        :param gamer:
        :param game:
        :param answer: номер ответа
        :return: (error: str, (Game, RoundMove)), RoundMove - объект ответа
        """
        if not 0 <= answer < 5:
            return 'Некорректный ответ', None
        try:
            game = await db_objects.get(Game, id=game)
        except Game.DoesNotExist:
            return 'Игра не найдена', None
        if game.end:
            return 'Игра уже завершилась', None
        if game.moving_user_id != gamer:
            return 'Не ваша очередь', None
        game_round = await db_objects.execute(
            RoundMove.select(RoundMove, Round, Question, Theme).join(Round).join(Question).join(Theme).where(
                (Round.game == game) &
                (RoundMove.answer >> None) &
                (RoundMove.gamer == gamer)
            ).limit(1)
        )
        try:
            return None, (game, game_round[0])
        except IndexError:
            return 'Вопрос не найден', None

    @staticmethod
    async def _get_total_score(game: Game, gamer: Gamer) -> int:
        """
        Получить общее число очков в игре
        :param game:
        :param gamer:
        :return:
        """
        return await db_objects.scalar(
            RoundMove.select(fn.sum(RoundMove.score)).where(
                (Round.game == game) &
                (RoundMove.gamer == gamer)
            ).join(Round)
        )

    @staticmethod
    async def _notify_old_recordsman(old_recordsman: Gamer, new_recordsman: Gamer, theme: Optional[Theme]) -> None:
        """
        PUSH-уведомление того, чей рекорд побит
        :param old_recordsman:
        :param new_recordsman:
        :param theme:
        """
        msg = f'в игре по теме "{theme.name}"' if theme else 'в абсолютном зачёте'
        msg = f'Ваш рекод {msg} побил {new_recordsman.name}'
        data = {
            'title': 'Увы, вы больше не рекордсмен',
            'msg': msg,
            'to_device': old_recordsman.device_token
        }
        try:
            async with aiohttp.ClientSession() as session:
                await session.post(
                    'http://localhost:4545/send_msg',
                    data=json.dumps(data)
                )
        except aiohttp.client.ClientConnectionError:
            pass

    @classmethod
    async def _create_move_single(cls, game: Game, game_round: Round, answer: int, correct: bool, game_over: bool,
                                  time_left: int, rating_obj: ThemeRating) -> None:
        """
        Асинхронная задача для создания хода одиночной игры в транзакции
        :param game:
        :param game_round:
        :param answer:
        :param correct:
        :param game_over:
        :param time_left:
        :param rating_obj:
        """
        question = game_round.round.question
        theme = question.theme
        gamer = game_round.gamer
        async with db_objects.atomic():
            game_round.answer = answer
            game_round.correct = correct
            game_round.timer_left = time_left
            score = game_round.timer_left * question.complexity if correct else 0
            game_round.score = score
            await db_objects.update(game_round)
            if not rating_obj:
                rating_obj = await db_objects.create(ThemeRating,
                                                     theme=game.theme,
                                                     gamer=gamer)

            if game_over:
                game.end = datetime.now()
                await db_objects.update(game)
                total_score = await cls._get_total_score(game, gamer)
                if total_score > rating_obj.record:
                    rating_obj.record = total_score
                    rating_obj.record_game = game.id
                try:
                    recordsman = await db_objects.get(RecordsMan, theme=game.theme)
                    if total_score > recordsman.score:
                        await cls._notify_old_recordsman(recordsman.gamer, gamer, game.theme)
                        recordsman.score = total_score
                        recordsman.gamer = gamer
                        recordsman.game = game
                        await db_objects.update(recordsman)
                except RecordsMan.DoesNotExist:
                    await db_objects.create(RecordsMan,
                                                   theme=game.theme,
                                                   gamer=gamer,
                                                   game=game,
                                                   score=total_score)
            rating_obj.total_answers += 1
            if score:
                rating_obj.score += score
            theme.total_answers += 1
            await db_objects.update(rating_obj)
            await db_objects.update(theme)

    @classmethod
    async def _create_move(cls, game: Game, game_round: Round, answer: str, time_left: int) -> bool:
        """
        Создание хода
        :param game:
        :param game_round:
        :param answer:
        :param time_left:
        :return: окончена ли игра
        """
        queue = await db_objects.execute(game.gamersqueue_set)
        is_multi = len(queue)
        correct = answer == game_round.round.question.correct
        loop = asyncio.get_event_loop()
        if is_multi:  # Многопользовательская
            pass
        else:  # Одиночная
            fails = await db_objects.scalar(
                RoundMove.select(fn.count(RoundMove.id)).join(Round).where(
                    (Round.game == game) &
                    ~RoundMove.correct
                )
            )
            game_over = fails + int(not correct) >= 3
            try:
                rating = await db_objects.get(ThemeRating, theme=game.theme,
                                              gamer=game_round.gamer_id)
            except ThemeRating.DoesNotExist:
                rating = None

            await loop.create_task(cls._create_move_single(game, game_round, answer, correct, game_over,
                                                           time_left, rating))

        return game_over

    async def post(self):
        data = self.data
        try:
            gamer = data['user']['id']
            game = data['game']
            answer = data['answer']
            time_left = data.get('timer_left', None)
        except KeyError as e:
            await self.error(f'Не указан {e.args[0]}')
            return

        error, res = await self._check(gamer, game, answer)
        if error:
            await self.error(error)
            return

        game, game_round = res
        game_over = await self._create_move(game, game_round, str(answer), time_left)
        await self.ok({
            'game_over': game_over
        })


class GetRoundMultiHandler(BaseHandler):
    @staticmethod
    async def _check(game_id, gamer_id, theme_id=None):
        try:
            game = await db_objects.get(Game, id=game_id)
            if game.end:
                return 'Игра уже закончена', None
            if game.moving_user_id != gamer_id:
                try:
                    await db_objects.get(GamersQueue,
                                         game=game_id,
                                         gamer=gamer_id)
                    return 'Не ваша очередь', None
                except GamersQueue.DoesNotExist:
                    return 'Вы не участвуете вы игре', None
            if theme_id:  # тема выбрана
                try:
                    await db_objects.get(Theme, id=theme_id)
                except Theme.DoesNotExist:
                    return 'Тема не найдена', None
            return None, game
        except Game.DoesNotExist:
            return 'Игра не найдена', None

    async def get(self):
        try:
            gamer_id = int(self.get_argument('user'))
            game_id = int(self.get_argument('game'))
        except MissingArgumentError as e:
            await self.error(f'Не указан {e.arg_name}')
            return
        except (ValueError, TypeError):
            await self.error('Неверный формат данных')
            return

        error, game = await self._check(game_id, gamer_id)
        if error:
            await self.error(error)
            return

        if game.status in {'s', 'w'}:
            themes = await db_objects.execute(
                Theme.select(Theme, fn.coalesce(ThemeRating.record, 0).alias('personal_score')).where(
                    Theme.name != 'Модерация'
                ).join(ThemeRating, JOIN.LEFT_OUTER, on=(
                    (ThemeRating.theme == Theme.id) &
                    (ThemeRating.gamer == gamer_id)
                    )).order_by(fn.random()).limit(3).dicts()
            )
            await self.ok(list(map(Theme.serialize, themes)))
        elif game.status == 'g':
            if game.moving_user_id == gamer_id:
                await self.ok(await NewGameHandler.get_rounds_multi(game))
            else:
                await self.error('Не ваша очередь')
        else:
            pass

    @staticmethod
    async def _is_my_round(gamer_id, game):
        if game.status == 's':
            return True
        return False

    @staticmethod
    async def _create_round_task(game, questions):
        round_number = 1 + await db_objects.scalar(
            Round.select(fn.coalesce(fn.max(Round.round_number), 0)).where(
                Round.game == game
            )
        )
        rows = [{
            'game': game.id,
            'round_number': round_number,
            'question': r['id']
        } for r in questions]
        async with db_objects.atomic():
            game.status = 'g'
            await db_objects.update(game)
            await db_objects.execute(
                Round.insert_many(rows)
            )
        return round_number

    @staticmethod
    def _reformat_questions(questions):
        exc_keys = ('theme', 'theme_id', 'theme_img_url')
        q = questions.pop()
        data = {
            'round_num': q['round_num'],
        }
        q['question']['id'] = q['id']
        q = q['question']
        data.update({
            'theme': q['theme'],
            'id': q['theme_id'],
            'img_url': q['theme_img_url']
        })
        for k in exc_keys:
            del q[k]
        data['questions'] = [q]
        while questions:
            q = questions.pop()
            q['question']['id'] = q['id']
            q = q['question']
            for k in exc_keys:
                del q[k]
            data['questions'].append(q)
        return data

    async def post(self):
        data = self.data
        try:
            gamer_id = data['user']['id']
            game_id = data['game']
            theme_id = data['theme']
        except KeyError as e:
            await self.error(f'Не указан {e.args[0]}')
            return

        error, game = await self._check(game_id, gamer_id, theme_id)
        if error:
            await self.error(error)
            return

        is_my_round = await self._is_my_round(gamer_id, game)
        if is_my_round:
            questions = await db_objects.execute(
                Question.select().where(
                    (Question.theme == theme_id) &
                    Question.active &
                    ~(Question.id << (
                        Round.select(Round.question).where(
                            Round.game == game_id
                        )
                    ))
                ).order_by(fn.random()).limit(3)
            )
            if not len(questions):
                await self.error('В теме не осталось вопросов')
                return

            formatter = partial(model_to_dict, recurse=False)
            questions_dict = list(map(formatter, questions))

            loop = asyncio.get_event_loop()
            # round_number = await loop.create_task(self._create_round_task(game, questions_dict))
            await loop.create_task(self._create_round_task(game, questions_dict))
            # formatter = partial(GetQuestionHandler.format_question, round_num=round_number)
            # questions = map(formatter, questions)
            await self.ok(list(map(Question.serialize, questions)))
        else:
            await self.error('Не ваша очередь')


class RoundMultiAnserHandler(BaseHandler):
    @staticmethod
    async def _get_round_number(game_id):
        return await db_objects.scalar(
            Round.select(fn.max(Round.round_number)).where(
                Round.game == game_id
            )
        )

    @classmethod
    async def _check(cls, gamer_id: int, game_id: int, answers: List[dict]) -> tuple:
        try:
            game = await db_objects.get(Game, id=game_id)
            if game.status != 'g':
                return 'Игра не найдена', None, None
            if game.moving_user_id != gamer_id:
                return 'Не ваша очередь', None, None
        except Game.DoesNotExist:
            return 'Игра не найдена', None, None
        if game.moving_user_id != gamer_id:
            return 'Не ваш ход', None, None
        error_answers = 'Неверные ответы'
        if len(answers) != 3:
            return error_answers, None, None
        answers_set = set()
        for item in answers:
            if not 0 <= item['answer'] <= 3:
                return error_answers, None, None
            if item['timer_left'] < 0:
                return error_answers, None, None
            answers_set.add(item['id'])
        round_number = await cls._get_round_number(game_id)
        div = await db_objects.execute(
            Round.select(Round.question).where(
                (Round.game == game_id) &
                (Round.question << list(answers_set)) &
                (Round.round_number == round_number)
            ).tuples()
        )
        div = answers_set - set(map(itemgetter(0), div))
        if div:
            return 'Не найдены вопросы %s' % ', '.join(list(map(str, div))), None, None
        return None, game, round_number

    @staticmethod
    async def _get_next_gamer(game, gamer_id, round_number, rounds):
        orders = await db_objects.execute(
            GamersQueue.select().where(GamersQueue.game == game)
        )
        my_order = 0 if orders[0].gamer_id == gamer_id else 1
        # Т.к. текущие ответы не встали в базу, то реализуем алгоритм наоборот
        current_round = rounds[round_number]
        if len(current_round) == 0:  # Ещё нет ответов, я выбирал тему, следующий - соперник
            if len(orders) == 2:
                return orders[1].gamer_id if my_order == 0 else orders[0].gamer_id
            else:
                return None  # Пока единственный в очереди
        # Ответы были, я выбираю тему
        return gamer_id

    @staticmethod
    async def _get_game_rounds(game):
        rows = await db_objects.execute(
            RoundMove.select(
                Round.round_number, RoundMove.gamer
            ).join(Round).where(Round.game == game).tuples()
        )
        rounds = defaultdict(set)
        for r in rows:
            rounds[r[0]].add(r[1])
        return rounds

    @staticmethod
    async def _round_ids(game, round_number):
        rounds = await db_objects.execute(
            Round.select(
                Round.question, Round.id, Question.correct, Question.complexity
            ).join(Question).where(
                (Round.game == game) &
                (Round.round_number == round_number)
            ).dicts()
        )
        return {r['question']: r for r in rounds}

    @classmethod
    async def _answer_taks(cls, game, gamer_id, round_number, answers):
        rounds = await cls._get_game_rounds(game)
        next_gamer = await cls._get_next_gamer(game, gamer_id, round_number, rounds)
        rounds_id = await cls._round_ids(game, round_number)

        def _is_correct(t):
            return t['answer'] == int(rounds_id[t['id']]['correct']) if t['timer_left'] else False

        rows = [{
            'round': rounds_id[r['id']]['id'],
            'gamer': gamer_id,
            'answer': r['answer'],
            'correct': _is_correct(r),
            'timer_left': r['timer_left'],
            'score': r['timer_left'] * rounds_id[r['id']]['complexity'] if _is_correct(r) else 0
        } for r in answers]
        async with db_objects.atomic():
            await db_objects.execute(
                RoundMove.insert_many(rows)
            )

            game_over = False
            if game.moving_user_id != next_gamer:
                game.moving_user = next_gamer
            else:
                if round_number == 3:
                    game.status = 'e'
                    game.end = datetime.now()
                    game_over = True
            if not game_over:
                if next_gamer is not None:
                    game.status = 'g' if next_gamer != gamer_id else 's'
                else:
                    game.status = 'w'
            await db_objects.update(game)
        return game_over, next_gamer == gamer_id

    async def post(self):
        data = self.data
        try:
            gamer_id = data['user']['id']
            game_id = data['game']
            answers = data['answers']
        except KeyError as e:
            await self.error(f'Не указан {e.args[0]}')
            return
        error, game, round_number = await self._check(gamer_id, game_id, answers)
        if error:
            await self.error(error)
            return
        game_over, your_round = await asyncio.get_event_loop().create_task(
            self._answer_taks(game, gamer_id, round_number, answers)
        )
        await self.ok({
            'game_over': game_over,
            'your_round': your_round
        })


class AddQuestionHandler(BaseHandler):
    async def post(self):
        MAX_LEN = 128
        data = self.data
        try:
            question = data['question']
            if len(question) > MAX_LEN:
                await self.error(f'Длина вопроса не должна превышать {MAX_LEN} символов')
                return
            a1 = data['answer_1']
            if len(a1) >MAX_LEN:
                await self.error(f'Длина ответа не должна превышать {MAX_LEN} символов')
                return
            a2 = data['answer_2']
            if len(a2) > MAX_LEN:
                await self.error(f'Длина ответа не должна превышать {MAX_LEN} символов')
                return
            a3 = data['answer_3']
            if len(a3) > MAX_LEN:
                await self.error(f'Длина ответа не должна превышать {MAX_LEN} символов')
                return
            a4 = data['answer_4']
            if len(a4) > MAX_LEN:
                await self.error(f'Длина ответа не должна превышать {MAX_LEN} символов')
                return
            correct = int(data['correct'])
            if correct < 1 or correct > 4:
                await self.error('Неверно указан правильный ответ')
                return
        except KeyError as e:
            await self.error(f'Не указан {e.args[0]}')
            return
        except TypeError:
            await self.error('Неверно указан правильный ответ')
            return

        moder_theme = await db_objects.get(Theme, name='Модерация')
        fields = {
            'theme': moder_theme,
            'question': question,
            'answer_1': a1,
            'answer_2': a2,
            'answer_3': a3,
            'answer_4': a4,
            'correct': correct,
            'active': False,
            'author': data['user']['id'],
            'lang': 'ru'
        }
        new_question = await db_objects.create(Question, **fields)
        await self.ok(new_question.id)
