from uuid import uuid4
from hashlib import sha256
from playhouse.shortcuts import model_to_dict
from base_handler import BaseHandler
from models import db_objects, Gamer


def get_hash(password):
    s = password + '3332aaa0-6bef-4092-b419-7106126472e8'
    r = sha256(s.encode('utf8')).hexdigest()
    return r


class RegisterHandler(BaseHandler):
    async def _check(self, email, name):
        try:
            user = await db_objects.execute(Gamer.select().where(
                (Gamer.email == email) |
                (Gamer.name == name)
            ).limit(1))
            user = user[0]
            if user.email == email:
                return 'Пользователь с таким email уже зарегистрирован'
            else:
                return 'Пользователь с таким имененм уже зарегистрирован'
        except IndexError:
            pass

    async def post(self):
        data = self.data
        try:
            error = await self._check(data['email'], data['name'])
            if error:
                await self.error(error)
                return
            gamer = {
                "name": data['name'],
                "email": data['email'],
                "passw": get_hash(data['password']),
                "device_token": data.get('device_token', None),
                "session": uuid4().hex,
                "os": data['device_os']
            }
            await db_objects.create(Gamer, **gamer)
            await self.ok(gamer['session'])
        except KeyError as e:
            await self.error(f'Не указан {e.args[0]}')


class LoginHandler(BaseHandler):
    async def _check(self, email, password):
        try:
            gamer = await db_objects.execute(Gamer.select().where(
                (Gamer.email == email) &
                (Gamer.passw == get_hash(password))
            ).limit(1))
            return None, gamer[0]
        except IndexError:
            return 'Пользователь с такими email и паролем не найден', None

    async def post(self):
        data = self.data
        try:
            error, gamer = await self._check(data['email'], data['password'])
            if error:
                await self.error(error)
                return
            gamer.device_token = data['device_token']
            gamer.session = uuid4().hex
            gamer.os = data['device_os']
            await db_objects.update(gamer)
            gamer = model_to_dict(gamer)
            del gamer['passw']
            await self.ok(gamer)
        except KeyError as e:
            await self.error(f'Не указан {e.args[0]}')


class CheckUserHandler(BaseHandler):
    async def post(self):
        data = self.data
        try:
            gamer = await db_objects.execute(Gamer.select().where(
                Gamer.session == data['session']
            ).dicts())
            gamer = gamer[0]
            del gamer['passw']
            await self.ok(gamer)
        except IndexError:
            await self.error('Пользователь не найден')
        except KeyError:
            await self.error('Пользователь не указан')
