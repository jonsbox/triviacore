import asyncio
import json
from tornado.web import RequestHandler


class BaseHandler(RequestHandler):
    @property
    def data(self):
        return json.loads(self.request.body)

    async def ok(self, result):
        self.write({
            'result': True,
            'data': result,
            'error': None
        })

    async def error(self, error):
        self.write({
            'result': False,
            'data': None,
            'error': error
        })
