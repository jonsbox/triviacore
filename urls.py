from gamers import handlers as hg
from game import handlers as g
from debug import main as dm


urls = (
    (r'/register', hg.RegisterHandler),
    (r'/login', hg.LoginHandler),
    (r'/check_user', hg.CheckUserHandler),

    (r'/start_app', g.StartAppHandler),
    (r'/new_game', g.NewGameHandler),

    (r'/get_question', g.GetQuestionHandler),
    (r'/answer', g.AnswerHandler),

    (r'/get_round', g.GetRoundMultiHandler),
    (r'/answer_round', g.RoundMultiAnserHandler),

    (r'/add_question', g.AddQuestionHandler),

    (r'/test_question', dm.RandomQuestion)
)