import unittest
import time
from datetime import datetime
from collections import defaultdict
from peewee import PostgresqlDatabase
from playhouse.shortcuts import model_to_dict
from models import Theme, Gamer, Question, Game, Round, RoundMove


test_db = PostgresqlDatabase(
    'test_trivia',
    user='test',
    password='test'
)

MODELS = [Theme, Gamer, Question, Game, Round, RoundMove]


class BaseTestCase(unittest.TestCase):
    def setUp(self) -> None:
        test_db.bind(MODELS, bind_refs=False, bind_backrefs=False)
        test_db.connect()
        test_db.create_tables(MODELS)

    def tearDown(self) -> None:
        test_db.drop_tables(MODELS)
        test_db.close()


def get_gamer(num):
    return Gamer.get_or_create(
        name=f'gamer {num}',
        email=f'email {num}',
        device_token=f'token {num}',
        passw=f'passw {num}',
        session=f'session {num}',
        os='winda'
    )[0]


def get_theme(num):
    return Theme.get_or_create(
        name=f'theme {num}',
        lang='ru',
        path_to_img='xxx'
    )[0]


def get_question(num, theme, gamer):
    return Question.get_or_create(
            theme=theme,
            question=f'question {num}',
            question_img=f'question_img {num}',
            answer_1='answer 1',
            answer_1_img='answer_1_img',
            answer_2='answer 2',
            answer_2_img='answer_2_img',
            answer_3='answer 3',
            answer_3_img='answer_3_img',
            answer_4='answer 4',
            answer_4_img='answer_4_img',
            correct='2',
            lang='ru',
            complexity=2,
            timer=15,
            active=True,
            author=gamer
        )[0]


class TestBaseObjects(BaseTestCase):
    def test_1_theme(self):
        theme = get_theme(1)
        expected = {
            'id': theme.get_id(),
            'name': 'theme 1',
            'path_to_img': 'xxx',
            'lang': 'ru'
        }
        self.assertEqual(expected, Theme.serialize(theme))
        theme = model_to_dict(theme, False)
        self.assertEqual(expected, Theme.serialize(theme))

        self.theme = theme['id']

    def test_2_gamer(self):
        gamer = get_gamer(1)
        expected = {
            'id': gamer.get_id(),
            'name': 'gamer 1'
        }
        self.assertEqual(expected, Gamer.serialize(gamer))
        gamer = model_to_dict(gamer, False)
        self.assertEqual(expected, Gamer.serialize(gamer))
        self.gamer = gamer['id']

    def test_3_question(self):
        theme = get_theme(1).get_id()
        gamer = get_gamer(1).get_id()
        question = get_question(1, theme, gamer)
        expected = {
            'id': question.get_id(),
            'theme': theme,
            'question': 'question 1',
            'question_img': 'question_img 1',
            'complexity': 2,
            'timer': 15,
            'answers': [
                {
                    'num': 1,
                    'answer': 'answer 1',
                    'answer_img': 'answer_1_img',
                    'correct': False
                },
                {
                    'num': 2,
                    'answer': 'answer 2',
                    'answer_img': 'answer_2_img',
                    'correct': True
                },
                {
                    'num': 3,
                    'answer': 'answer 3',
                    'answer_img': 'answer_3_img',
                    'correct': False
                },
                {
                    'num': 4,
                    'answer': 'answer 4',
                    'answer_img': 'answer_4_img',
                    'correct': False
                },
            ]
        }
        self.assertEqual(expected, Question.serialize(question))
        question = model_to_dict(question, False)
        self.assertEqual(expected, Question.serialize(question))


# TODO: переделать тесты под пустые раудны и под массив из /get_question
class TestSingleGame(BaseTestCase):
    start_game = datetime(2020, 10, 1, 14, 0)

    def init(self):
        self.theme = get_theme(1)
        self.gamer = get_gamer(1)

    def _create_game(self):
        return Game.get_or_create(
            start=self.start_game,
            moving_user=self.gamer.get_id(),
            theme=self.theme.get_id(),
        )[0]

    def test_1_new_game(self):
        self.init()
        game = self._create_game()
        expected = {
            'id': game.get_id(),
            'start': time.mktime(game.start.timetuple()),
            'end': None,
            'theme': {
                'id': self.theme.get_id(),
                'name': 'theme 1',
                'lang': 'ru',
                'path_to_img': 'xxx'
            },
            'gamers': [
                {
                    'id': self.gamer.get_id(),
                    'name': 'gamer 1',
                }
            ],
            'moving_gamer': self.gamer.get_id(),
            'multi': False,
            'rounds': None
        }
        gamer = game.moving_user
        theme = game.theme
        self.assertEqual(expected, Game.serialize(game, [gamer], theme))
        game = model_to_dict(game, False)
        self.assertEqual(expected, Game.serialize(game, [gamer], theme))

        game = Game.get(id=game['id'])
        game.theme = None
        game.save()
        expected['theme'] = None
        self.assertEqual(expected, Game.serialize(game, [gamer], None))
        game_dict = model_to_dict(game, False)
        self.assertEqual(expected, Game.serialize(game_dict, [gamer], None))

        game.theme = theme
        game.save()

    def _make_rounds(self):
        gamer = self.gamer.get_id()
        self.questions = {}
        self.moves = defaultdict(list)
        self.rounds = {}
        for (i, theme) in enumerate((self.theme, self.theme2), 1):
            question = get_question(i, theme, gamer)
            self.questions[i] = question
            r = Round.create(
                game=self.game.get_id(),
                round_number=i,
                question=question
            )
            self.rounds[i] = r
            m = RoundMove.create(
                round=r.id,
                gamer=gamer,
                answer=1,
                correct=False,
                timer_left=7,
                score=0
            )
            self.moves[i].append(m)
            m = RoundMove.create(
                round=r.id,
                gamer=gamer,
                answer=1,
                correct=False,
                timer_left=17,
                score=0
            )
            self.moves[i].append(m)

    def test_2_rounds_single(self):
        self.init()
        self.game = self._create_game()
        self.theme2 = get_theme(2)
        self._make_rounds()

        rounds = Round.get_rounds_query(self.game)

        gamer = get_gamer(1).get_id()
        mv = self.moves
        expected = [
            {
                'round_num': 1,
                'questions': [
                    Question.serialize(self.questions[1]),
                ],
                'answers': [{
                    'id': m.id,
                    'gamer': gamer,
                    'question': self.rounds[1].question.id,
                    'answer': m.answer,
                    'timer_left': m.timer_left,
                    'score': m.score
                } for m in mv[1]]
            },
            {
                'round_num': 2,
                'questions': [
                    Question.serialize(self.questions[2])
                ],
                'answers': [{
                    'id': m.id,
                    'gamer': gamer,
                    'question': self.rounds[2].question.id,
                    'answer': m.answer,
                    'timer_left': m.timer_left,
                    'score': m.score
                } for m in mv[2]]
            }
        ]
        self.assertEqual(expected, Round.serialize(rounds))


if __name__ == '__main__':
    unittest.main()
