import asyncio
import os
import logging.config
from tornado.web import Application
from tornado.platform.asyncio import AsyncIOMainLoop
from models import db_objects
from urls import urls


BASE_DIR = os.path.dirname(os.path.dirname(__file__))
LOGS_DIR = os.path.join(BASE_DIR, 'logs')
if not os.path.exists(LOGS_DIR):
    os.mkdir(LOGS_DIR)
LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'standard': {
            'format': "[%(asctime)s] %(levelname)s [%(name)s:%(lineno)s] %(message)s",
            'datefmt': "%d/%b/%Y %H:%M:%S"
        },
    },
    'handlers': {
        'null': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
        },
        'access': {
            'level': 'DEBUG',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': LOGS_DIR + "/access.log",
            'maxBytes': 50000,
            'backupCount': 3,
            'formatter': 'standard',
        },
        'app': {
            'level': 'DEBUG',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': LOGS_DIR + "/app.log",
            'maxBytes': 50000,
            'backupCount': 3,
            'formatter': 'standard',
        },
        'general': {
            'level': 'DEBUG',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': LOGS_DIR + "/general.log",
            'maxBytes': 50000,
            'backupCount': 3,
            'formatter': 'standard',
        },
        'debug': {
            'level': 'DEBUG',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': LOGS_DIR + "/debug.log",
            'maxBytes': 50000,
            'backupCount': 3,
            'formatter': 'standard',
        },
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'standard'
        },
    },
    'loggers': {
        "handlers": {
            "level": "DEBUG",
            "propagate": False,
            "handlers": ["debug", "console"]
        },
        "tornado.access": {
            "level": "INFO",
            "propagate": False,
            "handlers": ["access", "console"]
        },
        "tornado.application": {
            "level": "INFO",
            "propagate": False,
            "handlers": ["app", "console"]
        },
        "tornado.general": {
            "level": "INFO",
            "propagate": False,
            "handlers": ["general", "console"]
        },
    }
}
logging.config.dictConfig(LOGGING)


def main():
    AsyncIOMainLoop().install()
    app = Application(urls, debug=True)
    app.listen(port=4540)
    app.objects = db_objects
    logging.getLogger('tornado.general').info('Server started')
    asyncio.get_event_loop().run_forever()


if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        logging.getLogger('tornado.general').info('Server stopped')
