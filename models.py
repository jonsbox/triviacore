import time
from datetime import datetime
from peewee import Model, JOIN, Check, CharField, ForeignKeyField, SmallIntegerField, IntegerField, BooleanField
from playhouse.postgres_ext import DateTimeTZField
from playhouse.shortcuts import model_to_dict
from peewee_async import Manager, PostgresqlDatabase
from settings import DB_NAME, DB_USER, DB_PASS


db = PostgresqlDatabase(
    DB_NAME,
    user=DB_USER,
    password=DB_PASS
)
db_objects = Manager(db)
db.set_allow_sync(True)


class BaseModel(Model):
    class Meta:
        database = db


class Theme(BaseModel):
    name = CharField(verbose_name='Название', max_length=128)
    lang = CharField(max_length=2, verbose_name='Язык')
    path_to_img = CharField(null=True, verbose_name='Путь до изображения', max_length=256)
    # score = IntegerField(default=0, verbose_name='Счёт')
    total_answers = IntegerField(default=0, verbose_name='Количество ответов')

    def __str__(self):
        return self.name

    @staticmethod
    def serialize(data):
        if isinstance(data, BaseModel):
            data = model_to_dict(data, False)
        return {
            'id': data['id'],
            'name': data['name'],
            'path_to_img': data['path_to_img'],
            'lang': data['lang']
        }


class Gamer(BaseModel):
    name = CharField(max_length=256, unique=True)
    email = CharField(max_length=64, unique=True)
    device_token = CharField(max_length=64, null=True)
    passw = CharField(max_length=64)
    session = CharField(max_length=32, null=True, unique=True)
    os = CharField(max_length=16, null=True)

    @staticmethod
    def serialize(data):
        if isinstance(data, BaseModel):
            data = model_to_dict(data, False)
        return {
            'id': data['id'],
            'name': data['name'],
        }


class Question(BaseModel):
    theme = ForeignKeyField(Theme, on_delete='CASCADE', verbose_name='Тема')
    question = CharField(null=True, verbose_name='Вопрос', max_length=128)
    question_img = CharField(null=True, verbose_name='Путь до изображения', max_length=256)
    answer_1 = CharField(null=True, verbose_name='Ответ 1', max_length=128)
    answer_1_img = CharField(null=True, verbose_name='Путь до изображения', max_length=256)
    answer_2 = CharField(null=True, verbose_name='Ответ 2', max_length=128)
    answer_2_img = CharField(null=True, verbose_name='Путь до изображения', max_length=256)
    answer_3 = CharField(null=True, verbose_name='Ответ 3', max_length=128)
    answer_3_img = CharField(null=True, verbose_name='Путь до изображения', max_length=256)
    answer_4 = CharField(null=True, verbose_name='Ответ 4', max_length=128)
    answer_4_img = CharField(null=True, verbose_name='Путь до изображения', max_length=256)
    correct = CharField(max_length=1, verbose_name='Правильный ответ', choices=(
        ('1', '1'),
        ('2', '2'),
        ('3', '3'),
        ('4', '4')
    ))
    lang = CharField(max_length=2, verbose_name='Язык')
    complexity = SmallIntegerField(default=1, verbose_name='Сложность')
    timer = IntegerField(verbose_name='Таймер', default=15)
    active = BooleanField(default=True)
    author = ForeignKeyField(Gamer, null=True)

    class Meta:
        constraints = [
            Check('(((question IS NOT NULL) OR (question_img IS NOT NULL)) AND'
                  '((answer_1 IS NOT NULL) OR (answer_1_img IS NOT NULL)) AND'
                  '((answer_2 IS NOT NULL) OR (answer_2_img IS NOT NULL)) AND'
                  '((answer_3 IS NOT NULL) OR (answer_3_img IS NOT NULL)) AND'
                  '((answer_4 IS NOT NULL) OR (answer_4_img IS NOT NULL)))')
        ]

    def __str__(self):
        return self.question

    @staticmethod
    def serialize(data):
        if isinstance(data, BaseModel):
            data = model_to_dict(data, False)
        return {
            'id': data['id'],
            "theme": data['theme'],
            "question": data['question'],
            "question_img": data['question_img'],
            "timer": data['timer'],
            "complexity": data['complexity'],
            'answers': [{
                "num": n,
                "answer": data[f'answer_{n}'],
                "answer_img": data[f'answer_{n}_img'],
                "correct": str(n) == data['correct']
            } for n in range(1, 5)]
        }


class Game(BaseModel):
    start = DateTimeTZField(default=datetime.now)
    end = DateTimeTZField(null=True)
    moving_user = ForeignKeyField(Gamer, null=True)
    timer = IntegerField(null=True)
    theme = ForeignKeyField(Theme, null=True)
    overrecord_game = ForeignKeyField('self', null=True)
    status = CharField(max_length=1, null=True, choices=(
        ('w', 'Ожидание соперника'),
        ('s', 'Ожидание выбора темы'),
        ('g', 'Ожидание ответов'),
        ('e', 'Завершена')
    ))

    @staticmethod
    def serialize(game, gamers, theme=None, rounds=None):
        if isinstance(game, BaseModel):
            game = model_to_dict(game, False)
        return {
            'id': game['id'],
            'start': time.mktime(game['start'].timetuple()),
            'end': time.mktime(game['end'].timetuple()) if game['end'] else None,
            'theme': Theme.serialize(theme) if theme else None,
            'gamers': [Gamer.serialize(g) for g in gamers],
            'moving_gamer': game['moving_user'],
            'multi': game['status'] is not None,
            'rounds': Round.serialize(rounds) if rounds else None
        }


class Winners(BaseModel):
    game = ForeignKeyField(Game, on_delete='CASCADE')
    gamer = ForeignKeyField(Gamer)


class RecordsMan(BaseModel):
    theme = ForeignKeyField(Theme, unique=True, null=True)
    gamer = ForeignKeyField(Gamer)
    game = ForeignKeyField(Game, on_delete='CASCADE')
    score = IntegerField()


class ThemeRating(BaseModel):
    theme = ForeignKeyField(Theme)
    gamer = ForeignKeyField(Gamer)
    score = IntegerField(default=0)
    total_answers = IntegerField(default=0)
    record = IntegerField(default=0)
    record_game = ForeignKeyField(Game, null=True, on_delete='CASCADE')


class GamersQueue(BaseModel):
    game = ForeignKeyField(Game, on_delete='CASCADE')
    gamer = ForeignKeyField(Gamer)
    order = IntegerField()


class Round(BaseModel):
    game = ForeignKeyField(Game, on_delete='CASCADE')
    round_number = SmallIntegerField()
    question = ForeignKeyField(Question)

    @staticmethod
    def serialize(rounds):
        res = {}
        serialized_questions = set()
        for r in rounds:
            rn = r.round_number
            if rn not in res:
                res[rn] = {
                    'round_num': rn,
                    'questions': [],
                    'answers': []
                }
                serialized_questions = set()
            if r.question.id not in serialized_questions:
                res[rn]['questions'].append(Question.serialize(r.question))
                serialized_questions.add(r.question.id)
            if not hasattr(r, 'roundmove'):
                continue
            m = r.roundmove
            if m.answer is None:
                continue
            res[rn]['answers'].append({
                'id': m.id,
                'gamer': m.gamer.id,
                'question': r.question.id,
                'answer': m.answer,
                'timer_left': m.timer_left,
                'score': m.score
            })
        return [res[k] for k in sorted(res.keys())]

    @staticmethod
    def get_rounds_query(game):
        return Round.select(
            Round, RoundMove, Question, Gamer
        ).join(Question).switch(Round).join(RoundMove, JOIN.LEFT_OUTER).join(Gamer, JOIN.LEFT_OUTER).where(
            Round.game == game
        ).order_by(Round.round_number, RoundMove.id)


class RoundMove(BaseModel):
    round = ForeignKeyField(Round, on_delete='CASCADE')
    gamer = ForeignKeyField(Gamer)
    answer = SmallIntegerField(null=True)
    correct = BooleanField(null=True)
    timer_left = IntegerField(null=True)
    score = IntegerField(default=0)
