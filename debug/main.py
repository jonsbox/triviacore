from random import randint
from base_handler import BaseHandler
from models import db_objects, Question


class RandomQuestion(BaseHandler):
    async def get(self):
        questions = await db_objects.execute(Question.select().dicts())
        await self.ok({
            'result': True,
            'error': None,
            'data': questions[randint(0, len(questions) - 1)]
        })
