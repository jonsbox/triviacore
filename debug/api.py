Theme = {
    'id': 12,
    'name': 'xxx',
    'lang': 'ru'
}

AnswerItem = {
    'num': 1,
    'answer': 'hz',
    'answer_img': None,
    'correct': True
}

Question = {
    'id': 1,
    'theme': 12,
    'question': 'wtf?',
    'question_img': None,
    'complexity': 1,
    'timer': 7777,
    'answers': [AnswerItem, AnswerItem, AnswerItem, AnswerItem]
}

Gamer = {
    'id': 2,
    'name': 'myname'
}

RoundItem = {
    'id': 11257,
    'gamer': 2,
    'question': 71,
    'answer': 773,
    'time_left': 7,
    'score': 52
}

Round = {
    'round_num': 2,
    'questions': [Question, Question],
    'answers': [RoundItem, RoundItem]
}

Game = {
    'id': 11,
    'start': 99700738432524,
    'end': None,
    'theme': Theme,  # or None,
    'gamers': [Gamer, Gamer],  # [Gamer] для однопользовательской
    'multi': True,  # False для однопользовательской
    'rounds': [Round, Round, Round]
}
